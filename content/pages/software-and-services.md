+++
date = "2012-08-05T19:41:00-05:00"
draft = true
menu = ["main", "footer"]
noauthor = true
nocomment = true
nodate = true
nopaging = true
noread = true
title = "Software and Services"

+++

In no particular order, I endorse the following software, products and services
on a personal level:
<!--more-->
# OSX Software

* [Adium](http://adium.im/)
* [Sparrow](http://sprw.me/) - no new development due to acquisition by Google is heartbreaking
* [GitHub OSX client](http://mac.github.com)
* [Reeder](http://reederapp.com/mac/) - Best RSS reader I've ever used, and it has
  a fantastic [iOS port](http://reederapp.com/ipad/)!
* [TweetBot](http://tapbots.com/tweetbot_mac/) - Originally on
  [iOS](http://tapbots.com/software/tweetbot/), and is far and away my preferred Twitter client on both platforms. Beats the hell out of TweetDeck.
* [iTerm 2](http://www.iterm2.com)
* [tmux](http://tmux.sourceforge.net) - not OSX specific, but where I use it most

# Cross-platform Software

* [Dropbox](http://dropbox.com/) - still one of my favorite ways to keep files
  available on all my machines, including the iPad. Bonus: integration with
  [PragProg](http://pragprog.com/frequently-asked-questions/ebooks/read-on-desktop-laptop#dropbox)
  and [O'Reilly](http://shop.oreilly.com/category/customer-service/dropbox.do)
  for near-instant notificaiton of new e-book revisions!
* [SublimeText 2](http://sublimetext.com/2) - see my 1-month impressions
  [here](/posts/2012-08-05-sublimetext-2/)!
* [Spotify](http://spotify.com/) - the only streaming music service I'm still
  consistently glad I subscribe to. Works like a charm on Windows and OSX, and
  even has a [Linux port](http://spotify.com/us/download/previews/)!
* [Amazon Kindle](https://kindle.amazon.com) - my preferred e-reader platform
  because it has great dedicated
  [hardware readers](https://www.amazon.com/kindle-store-ebooks-newspapers-blogs/b/ref=r_ksl_h_i_gl?node=133141011),
  and a reading app for pretty much
  [every platform under the sun](https://www.amazon.com/gp/kindle/kcp/ref=r_kala_h_i_gl).

# Cloud/Web Apps

* [Heroku](http://heroku.com/)
* [Mint](http://mint.com/)
* [Pinboard](http://pinboard.in/) - bookmarking service *ala* Delicious but
  without all the social trappings - and with great integration in Twitter clients/RSS readers!
* [Pandora](http://pandora.com/) - I'm less happy with their variety than I wish, but still a great service

# Screencasts

* [Railscasts](http://railscasts.com/) - Ryan Bates has been consistently
  putting out focused, concise and informative screencasts on a variety of topics
  related to Ruby on Rails or programming at large. Congrats on a full year of
  doing RailsCasts full-time, Ryan! Happy to be a Pro subscriber.
* [PeepCode](https://peepcode.com/) - May be less prolific than Railscasts,
  but the screencasts Geoffrey puts out are very meaty and I'm a big fan of the
  [Play by Play series](https://peepcode.com/products/play-by-play-tenderlove-ruby-on-rails).

# Podcasts

* [Ruby5](http://ruby5.envylabs.com/) is a great way to keep abreast of
  new or upcoming projects from the Ruby ecosystem
* [Ruby Rogues](http://rubyrogues.com/) isn't nearly so bite-sized but dives deeper
  into subjects and often has very interesting guests

# Reference Books

* [Pragmatic Programmers](http://pragprog.com/) keeps publishing awesome books
  from great authors on exciting topics
* [Manning](http://www.manning.com/) - I still buy the odd
  [MEAP book](http://www.manning.com/about/meap) on an upcoming technology and
  have yet to regret it
* O'Reilly publishes most new books in an ebook format and sports integration
  with Dropbox for delivery of updated versions

# E-Book Vendors

* [DriveThruRPG](http://rpg.drivethrustuff.com/) has awesome sales on tabletop
  RPG rulebooks now and then - I've got a ton of
  [old](http://www.white-wolf.com/classic-world-of-darkness)/
  [new World of Darkness](http://www.white-wolf.com/new-world-of-darkness)
  and [Shadowrun](http://shadowrun4.com/), some Exalted/Trinity, and a dash
  of [Pathfinder](http://paizo.com/).

